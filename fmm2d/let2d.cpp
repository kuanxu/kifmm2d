/* Kernel Independent Fast Multipole Method
   Copyright (C) 2004 Lexing Ying, New York University

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING.  If not, write to the Free
Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.  */
#include "let2d.hpp"

using std::min;
using std::max;
using std::set;
using std::queue;
using std::ofstream;
using std::cerr;
using std::endl;
using std::istringstream;

//-----------------------------------------------------------------------
Let2d::Let2d(const string& p):
  ComObject(p), _srcPos(NULL), _trgPos(NULL), _center(0.0), _rootLevel(0),
  _ptsMax(150), _maxLevel(10)
{
}

Let2d::~Let2d()
{
}

// ---------------------------------------------------------------------- 
int Let2d::setup(map<string,string>& optionsMap)  // pass the info in optionsMap to let2d
{
  //------------
  map<string,string>::iterator mapindex;
  mapindex = optionsMap.find("-" + prefix() + "ptsMax"); iA(mapindex!=optionsMap.end());
  { istringstream ss((*mapindex).second); ss>>_ptsMax; }
  mapindex = optionsMap.find("-" + prefix() + "maxLevel"); iA(mapindex!=optionsMap.end());
  { istringstream ss((*mapindex).second); ss>>_maxLevel; }
  //------------
  iC( srcData() );   // call Let2d::srcData
  iC( trgData() );   // call Let2d::trgData
  return 0;
}

// ---------------------------------------------------------------------- 
int Let2d::srcData()
{
  //-----------------------------------------
  ///gdata
  DblNumMat& pos = *(_srcPos);  iA( pos.m()==dim() );  // pass all the source points in let2d.hpp to pos
  
  //IMPORTANT: nodeVec, VecIdxs, and lclSrcNumVec share the same index!!!
  vector<Node>& nodeVec = this->nodeVec(); nodeVec.clear();  // nodeVec is the vector saving all nodes/boxes and nodeVec is a copy of _nodeVec
  vector< vector<int> > vecIdxs;   // each vector<int> saves the indice of source position in matrix [pos], which are actually the column indice of [pos]
  //local src number, the number of src point in each box
  vector<int> lclSrcNumVec; //each entry corresponds to the total number of source points in each node/box. Each entry in this vector corresponds to each vector<int> in vecIdxs
  
  nodeVec.push_back( Node(-1,-1, Index2(0,0), 0) );  // the root
  vecIdxs.push_back( vector<int>() );  // push in empty vector of integers for next coming-in node/box
  vector<int>& curVecIdxs = vecIdxs[0];  // index of source points in matrix [pos] in current node/box (the root level). curVecIdxs is just an alias.
  Point2 bbmin(center()-Point2(radius()));  //set the upper bound. Now it's the upper bound of the node/box in rootlevel. 
  Point2 bbmax(center()+Point2(radius()));  //set the lower bound. Now it's the lower bound of the node/box in rootlevel. 
  
  // check each source point to see if it is inside the current box. If so, push its index into curVecIdxs. 
  for(int k=0; k<pos.n(); k++) {
	 Point2 tmp(pos.clmdata(k));	 
	 iA(tmp>=bbmin && tmp<=bbmax);
	 curVecIdxs.push_back(k);
  }
  lclSrcNumVec.push_back( curVecIdxs.size() );  // total the number of the source points in the current node/box (the root level)
  
  int level = 0;
  int arrBeg = 0;  // the first index of boxes in current level
  int arrEnd = 1;  // the first index of boxes in the next level
  int arrCnt = 0;
  while(arrBeg < arrEnd) {  // loop over each level
	 //1.  
	 arrCnt = arrEnd;  // index to the current end of nodeVec. The end of nodeVec is changing as nodeVec is growing!!!
	 for(int k=arrBeg; k < arrEnd; k++) {  // k runs over all boxes in current level. IMPORTANT: k is the index i n nodeVec, the index in VecIdxs, and lclSrcNumVec!!!
		//---
		if( lclSrcNumVec[k]>ptsMax() && level<maxLevel()-1 ) { //if the number of the source points is larger than the upper limit and the max level is not reached. 
		  nodeVec[k].child() = arrCnt;  // arrCnt is the starting index of current node's children
		  arrCnt = arrCnt + pow2(dim());  // four more nodes will be added to nodeVec
		  //children's essential tree		  
		  for(int a=0; a<2; a++) {
			 for(int b=0; b<2; b++) {
				  nodeVec.push_back( Node(k,-1, 2*nodeVec[k].path2Node()+Index2(a,b), nodeVec[k].depth()+1) ); // push the new nodes in the next level into nodeVec
				  vecIdxs.push_back( vector<int>() );
				  lclSrcNumVec.push_back(0);
			 }
		  }
		  //children's vector of indices
		  Point2 centerCurNode( center(k) ); //get center of current node
		  for(vector<int>::iterator vecIdxsIt=vecIdxs[k].begin(); vecIdxsIt!=vecIdxs[k].end(); vecIdxsIt++) {  // loop over the source points in box k of nodeVec
			 Point2 tmp(pos.clmdata(*vecIdxsIt)); //*vecIdxsIt is the column index used in source position matrix [pos]
			 Index2 idx;
			 for(int j=0; j<dim(); j++){
				idx(j) = (tmp(j) >= centerCurNode(j));
			 }
			 int chdGNodeIdx = child(k, idx);  // get children's index in nodeVec
			 vecIdxs[chdGNodeIdx].push_back(*vecIdxsIt);   // insert column index of source points in [pos] into the correct vector<int> of vecIdxs corresponding to proper children nodes/boxes
		  }
		  vecIdxs[k].clear(); //VERY IMPORTANT  Always free the space used for storing the points distribution in current box k
		  //children's local source number		  
		  for(int a=0; a<2; a++) {
			 for(int b=0; b<2; b++) {
				  int chdGNodeIdx = child( k, Index2(a,b) );
				  lclSrcNumVec[chdGNodeIdx] = vecIdxs[chdGNodeIdx].size();  // compute the total number of source points in each child node/box and then save it into the corresponding entry of lclSrcNumVec
			 }
		  }
		}
	 }
	 level++; // this "level" is used in the if condition at the very beginning of the outer "for" loop.
	 arrBeg = arrEnd;
	 arrEnd = arrCnt;
  }
  _level = level; //SET LEVEL

  //ordering of the boxes, in top-down or bottom-up fashion
  vector<int> orderBoxesVec	;  iC( dwnOrderCollect(orderBoxesVec) );  // build the index vector
  //set other parts of essvec
  int cnt = 0;
  int sum = 0;
  for(int i=0; i < orderBoxesVec.size(); i++) {
	 int gNodeIdx = orderBoxesVec[i];
	 if(lclSrcNumVec[gNodeIdx]>0) {  // It's possible that some nodes/boxes do not have any source points! 
		nodeVec[gNodeIdx].tag() = nodeVec[gNodeIdx].tag() | LET_SRCNODE;  // the operator "|" is used to set the tag()
		nodeVec[gNodeIdx].srcNodeIdx() = cnt; //  give each node an index
		cnt++;
		if(nodeVec[gNodeIdx].child()==-1) {  // if this node is a leaf, fill all the corresponding information into the proper node object
		  nodeVec[gNodeIdx].srcExaBeg() = sum;
		  nodeVec[gNodeIdx].srcExaNum() = lclSrcNumVec[gNodeIdx];
		  sum += lclSrcNumVec[gNodeIdx];
		  nodeVec[gNodeIdx].srcOwnVecIdxs() = vecIdxs[gNodeIdx];
		}
	 }
  }
  _srcNodeCnt = cnt;  // total number of the nodes (both leaf and non-leaf) which have at least one source point
  _srcExaCnt = sum; // total number of the source points
  return 0;
}

// ---------------------------------------------------------------------- 
int Let2d::trgData()
{
  //-----------------------------------------
  //edata
  DblNumMat& pos = *(_trgPos);  iA( pos.m()==dim() );
  
  vector<Node>& nodeVec = this->nodeVec();
  vector< vector<int> > vecIdxs; vecIdxs.resize(nodeVec.size() );
  vector<int> lclSrcNumVec;           lclSrcNumVec.resize(nodeVec.size(), 0);
  
  vector<int>& curVecIdxs = vecIdxs[0];
  Point2 bbmin(center()-Point2(radius()));
  Point2 bbmax(center()+Point2(radius()));  //cerr<<" bbx "<<bbmin<<" "<<bbmax<<endl;
  for(int k=0; k < pos.n(); k++) {
	 Point2 tmp(pos.clmdata(k));
	 iA(tmp>=bbmin && tmp<=bbmax);	 //LEXING: IMPORTANT
	 curVecIdxs.push_back(k);
  }
  lclSrcNumVec[0] = curVecIdxs.size();
  
  vector<int> orderBoxesVec;  iC( dwnOrderCollect(orderBoxesVec) );
  for(int i=0; i < orderBoxesVec.size(); i++) {
	 int gNodeIdx = orderBoxesVec[i];
	 Node& curNode = nodeVec[gNodeIdx]; // current node
	 vector<int>& curVecIdxs = vecIdxs[gNodeIdx];	 
	 if(curNode.child()!=-1) { //not terminal
		//children's vecIdxs
		Point2 curCenter( center(gNodeIdx) );
		for(vector<int>::iterator curVecIdxsIt=curVecIdxs.begin();curVecIdxsIt !=curVecIdxs.end(); curVecIdxsIt++) {
		  Point2 tmp(pos.clmdata(*curVecIdxsIt));
		  Index2 idx;
		  for(int j=0; j<dim(); j++)
			 idx(j) = (tmp(j)>=curCenter(j));
		  int chdGNodeIdx = child(gNodeIdx, idx);
		  vector<int>& chdVecIdxs = vecIdxs[chdGNodeIdx];
		  chdVecIdxs.push_back(*curVecIdxsIt);
		}
		curVecIdxs.clear(); //VERY IMPORTANT
		//children's lsm		//		for(int ord=0; ord<8; ord++) {
		for(int a=0; a<2; a++) {
		  for(int b=0; b<2; b++) {
				int chdGNodeIdx = child(gNodeIdx, Index2(a,b));
				lclSrcNumVec[chdGNodeIdx] = vecIdxs[chdGNodeIdx].size();
		  }
		}
	 }
  }
  // set the evaluation tree
  int cnt = 0;
  int sum = 0;
  for(int i=0; i<orderBoxesVec.size(); i++) {
	 int gNodeIdx = orderBoxesVec[i];
	 if(lclSrcNumVec[gNodeIdx]>0) { //evaluation tree node
		nodeVec[gNodeIdx].tag() = nodeVec[gNodeIdx].tag() | LET_TRGNODE;
		nodeVec[gNodeIdx].trgNodeIdx() = cnt; //  give each node an index
		cnt ++;
		if(nodeVec[gNodeIdx].child()==-1) { //terminal
		  nodeVec[gNodeIdx].trgExaBeg() = sum;
		  nodeVec[gNodeIdx].trgExaNum() = lclSrcNumVec[gNodeIdx];
		  sum += lclSrcNumVec[gNodeIdx];
		  nodeVec[gNodeIdx].trgOwnVecIdxs() = vecIdxs[gNodeIdx];
		}
	 }
  }
  _trgNodeCnt = cnt;  _trgExaCnt = sum;
  
  //set USER
  for(int i=0; i < orderBoxesVec.size(); i++) {
	 int gNodeIdx = orderBoxesVec[i];
	 if(nodeVec[gNodeIdx].tag() & LET_TRGNODE) { //a evtr		
		iC( calgnext(gNodeIdx) );
	 }
  }
  //cerr<<usndecnt<<" "<<usextcnt<<endl;
  return 0;
}

// ---------------------------------------------------------------------- 
int Let2d::calgnext(int gNodeIdx)  // calculate the interaction list U, V, W, and X
{
  vector<Node>&  nodeVec = this->nodeVec();
  
  set<int> Uset, Vset, Wset, Xset;
  int curGNodeIdx = gNodeIdx;
  if(root(curGNodeIdx)==false) {
	 int parGNodeIdx = parent(curGNodeIdx);  // parent's index in nodeVec
	 
	 Index2 minIdx(0);   // minimum index in current level
	 Index2 maxIdx(pow2(depth(curGNodeIdx)));  // maximum index in current level

	 for(int i=-2; i<4; i++) {
		for(int j=-2; j<4; j++)	{  // loop over all the boxes in the interaction list
			 Index2 tryPath( 2*path2Node(parGNodeIdx) + Index2(i,j) );
			 if(tryPath >= minIdx && tryPath <  maxIdx && tryPath != path2Node(curGNodeIdx)) {	
				int resGNodeIdx = findgnt(depth(curGNodeIdx), tryPath);  // knowing the depth of curgni, find the trypath's index in ndevec
				bool adj = adjacent(resGNodeIdx, curGNodeIdx);  // check if resgni and curgni are adjacent
				if( depth(resGNodeIdx) < depth(curGNodeIdx) ) {  // if the current node is deeper than the trypath node
				  if(adj){  // if they are adjacent
					 if(terminal(curGNodeIdx)){  // if current node is a leaf
						Uset.insert(resGNodeIdx);
					 }
					 else { ; }
				  }
				  else {
					 Xset.insert(resGNodeIdx);
				  }
				}
				if( depth(resGNodeIdx)==depth(curGNodeIdx) ) {  // if resgni and curgni are in the same level
				  if(!adj) {  // if resgni and curgni are not adjacent 
					 Index2 bb(path2Node(resGNodeIdx)-path2Node(curGNodeIdx));
					 assert( bb.linfty()<=3 );
					 Vset.insert(resGNodeIdx);
				  }
				  else {  // if resgni and curgni are adjacent and curgni is a leaf
					 if(terminal(curGNodeIdx)) {
						queue<int> rest;
						rest.push(resGNodeIdx);
						while(rest.empty()==false) {
						  int fntGNodeIdx = rest.front(); 
                                                  rest.pop();	// after using it, delete it from the queue by pop()				 
                                                  //int fntgNodeIdx = fntgnt.gNodeIdx();
						  if(adjacent(fntGNodeIdx, curGNodeIdx)==false) {
							 Wset.insert( fntGNodeIdx );
						  }
						  else {
							 if(terminal(fntGNodeIdx)) {
								Uset.insert(fntGNodeIdx);
							 }
							 else { 
								for(int a=0; a<2; a++) {
								  for(int b=0; b<2; b++) {
										rest.push( child(fntGNodeIdx, Index2(a,b)) );
								  }
								}
							 }
						  }
						}
					 }
				  }
				}
			 }
		}
	 }
  }
  if(terminal(curGNodeIdx))
	 Uset.insert(curGNodeIdx);
  
  for(set<int>::iterator si=Uset.begin(); si!=Uset.end(); si++)
	 if(nodeVec[*si].tag() & LET_SRCNODE)		nodeVec[gNodeIdx].Unodes().push_back(*si);
  for(set<int>::iterator si=Vset.begin(); si!=Vset.end(); si++)
	 if(nodeVec[*si].tag() & LET_SRCNODE)		nodeVec[gNodeIdx].Vnodes().push_back(*si);
  for(set<int>::iterator si=Wset.begin(); si!=Wset.end(); si++)
	 if(nodeVec[*si].tag() & LET_SRCNODE)		nodeVec[gNodeIdx].Wnodes().push_back(*si);
  for(set<int>::iterator si=Xset.begin(); si!=Xset.end(); si++)
	 if(nodeVec[*si].tag() & LET_SRCNODE)		nodeVec[gNodeIdx].Xnodes().push_back(*si);
  
  return (0);
}
// ---------------------------------------------------------------------- 
int Let2d::dwnOrderCollect(vector<int>& orderBoxesVec)
{
  orderBoxesVec.clear();
  for(int i=0; i < nodeVec().size(); i++)
	 orderBoxesVec.push_back(i);
  iA(orderBoxesVec.size()==nodeVec().size());
  return 0;
}
// ---------------------------------------------------------------------- 
#undef __FUNCT__
#define __FUNCT__ "Let2d::upwardOrderCollect"
int Let2d::upwOrderCollect(vector<int>& orderBoxesVec)
{
  orderBoxesVec.clear();
  for(int i=nodeVec().size()-1; i>=0; i--)
	 orderBoxesVec.push_back(i);
  iA(orderBoxesVec.size()==nodeVec().size());
  return 0;
}
// ---------------------------------------------------------------------- 
int Let2d::child(int gNodeIdx, const Index2& idx)
{
  assert(idx>=Index2(0) && idx<Index2(2));  // bound checking
  return node(gNodeIdx).child() + (idx(0)*2+idx(1));  // child's index
}
Point2 Let2d::center(int gNodeIdx) //center of a node。 gNodeIdx is the index in nodeVec
{
  Point2 ll( center() - Point2(radius()) );  // ll is the point (-1, -1)
  int tmp = pow2(depth(gNodeIdx));   // rad at current level is 2*rad()/tmp
  Index2 pathLcl(path2Node(gNodeIdx));   // pathLcl is the path of the current node with index gNodeIdx. gNodeIdx is the index in nodeVec
  Point2 res;
  for(int d=0; d<dim(); d++) {
	 res(d) = ll(d) + (2*radius()) * (pathLcl(d)+0.5) / double(tmp);  // the coordinates of the center of the current node with its "path index" pass
  }
  return res;
}
double Let2d::radius(int gNodeIdx) //radius of a node
{
  return radius()/double(pow2(depth(gNodeIdx)));
}
// ---------------------------------------------------------------------- 
int Let2d::findgnt(int wntdepth, const Index2& wntpath)  // given a depth and an 2-entry index, calculate the corresponding index in nodeVec. The node corresponding to wntpath is shallower or as deep as wntdepth
{
  int cur = 0; // start from the first node in ndevec which is the root 
  //cerr<<"GOOD "<<path(cur)<<"     ";
  Index2 leftpath(wntpath);
  while(depth(cur)<wntdepth && terminal(cur)==false) {
	 int dif = wntdepth-depth(cur);
	 int tmp = pow2(dif-1);
	 Index2 choice( leftpath/tmp );  // choice is the 2-entry index in each level counting down from the root thru the current level
	 leftpath -= choice*tmp;   // leftpath is the residual.
	 cur = child(cur, choice); // cur is the index in nodeVec 
         //cur = child(cur, IdxIter(0,2,true,choice) );	 //cerr<<path(cur)<<"["<<choice<<" "<<tmp<<"]"<<"     ";
  }  //cerr<<endl;
  return cur;
}
// ---------------------------------------------------------------------- 
bool Let2d::adjacent(int me, int yo)
{  // note that the 2-entry index is actually the coordinate of the lower-left corner of each box 
  int md = max(depth(me),depth(yo));  // the deeper depth
  Index2 one(1);  // the coordinate difference between the lower-left corner of a box and the center of this box
  Index2 mecenter(  (2*path2Node(me)+one) * pow2(md - depth(me))  );  // the index for the center of me
  Index2 yocenter(  (2*path2Node(yo)+one) * pow2(md - depth(yo))  );  // the index for the center of yo
  int meradius = pow2(md - depth(me));  // radius of me
  int yoradius = pow2(md - depth(yo));  // radius of yo
  Index2 dif( abs(mecenter-yocenter) );  // distance between their centers
  int radius  = meradius + yoradius;   // maximum distance between their centers for them to be separated and adjacent
  return
	 ( dif <= Index2(radius) ) && //not too far
	 ( dif.linfty() == radius ); // they at least touch at an edge or a corner
}

// ---------------------------------------------------------------------- 
int Let2d::print()
{
  cerr<<_nodeVec.size()<<" "<<_level<<endl;
  cerr<<_srcNodeCnt <<" "<< _srcExaCnt << endl;
  cerr<<_trgNodeCnt <<" "<< _trgExaCnt << endl;

  for(int i=0; i<_nodeVec.size(); i++) {
	 cerr<<node(i).parent()<<" "<<node(i).child()<<endl;
  }
  
  for(int i=0; i<_nodeVec.size(); i++) {
	 //cerr<<node(i).Unodes().size()<<" "<<node(i).Vnodes().size()<<" "<<node(i).Wnodes().size()<<" "<<node(i).Xnodes().size()<<" "<<endl;
	 Node& n = node(i);
	 cerr<<n.srcNodeIdx()<<" "<<n.srcExaBeg()<<" "<<n.srcExaNum()<<" "
		  <<n.trgNodeIdx()<<" "<<n.trgExaBeg()<<" "<<n.trgExaNum()<<endl;
  }
  return 0;
}
