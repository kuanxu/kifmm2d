/* Kernel Independent Fast Multipole Method
   Copyright (C) 2004 Lexing Ying, New York University

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING.  If not, write to the Free
Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.  */
#include "fmm2d.hpp"

using std::cerr;
using std::endl;

using std::istringstream;

// ---------------------------------------------------------------------- 
int FMM2d::setup(map<string,string>& opts)
{
  //-----------------------------------------------------
  map<string,string>::iterator mi;
  mi = opts.find("-" + prefix() + "np"); iA(mi!=opts.end());
  { istringstream ss((*mi).second); ss>>_np; }
    
  //-----------------------------------------------------
  iA(_srcPos!=NULL && _srcNor!=NULL && _trgPos!=NULL);
  iA((*_srcPos).m()==dim() && (*_trgPos).m()==dim());  //nothing to do
  
  //1. build the local essential tree _let
  _let = new Let2d(prefix()+"let2d_");
  _let->srcPos()=_srcPos;  // pass the _srcPos of KnlMat2d to that of _let
  _let->trgPos()=_trgPos;  // pass the _trgPos of KnlMat2d to that of _let
  _let->center()=_center;  // pass the _center of KnlMat2d to that of _let
  _let->rootLevel()=_rootLevel; // pass the _rootlevel of fmm2d to that of _let
  
  iC( _let->setup(opts) );  // set up the local essential tree using _srcPos, _trgPos, _center, and _rootlevel.
  
  //2. decide _eq_mm and _mul_mm, and get matmgnt based on that
  switch(_knl.kernelType()) {  // pass the right kernel from _knl of knlmat2d to _knl_mm of fmm2d
	 //laplace kernels
  case KNL_LAP_S_U: _knl_mm = Kernel2d(KNL_LAP_S_U, _knl.coefs()); break;
  case KNL_LAP_D_U: _knl_mm = Kernel2d(KNL_LAP_S_U, _knl.coefs()); break;
	 //stokes kernels
  case KNL_STK_S_U: _knl_mm = Kernel2d(KNL_STK_S_U, _knl.coefs()); break;
  //case KNL_STK_S_P: _knl_mm = Kernel2d(KNL_LAP_S_U, vector<double>()); break;
  case KNL_STK_D_U: _knl_mm = Kernel2d(KNL_STK_F_U, _knl.coefs()); break;
  //case KNL_STK_D_P: _knl_mm = Kernel2d(KNL_LAP_S_U, vector<double>()); break;
	 //navier kernels
  //case KNL_NAV_S_U: _knl_mm = Kernel2d(KNL_NAV_S_U, _knl.coefs()); break;
  //case KNL_NAV_D_U: _knl_mm = Kernel2d(KNL_NAV_S_U, _knl.coefs()); break;
	 //others
  //case KNL_SQRTLAP: _knl_mm = Kernel2d(KNL_SQRTLAP, _knl.coefs()); break;
  //case KNL_EXP    : _knl_mm = Kernel2d(KNL_EXP    , _knl.coefs()); break;
  default: iA(0);
  }
  _mul_mm = 1; //for the time being
  
  _matmgnt  = MatMgnt2d::getmmptr(_knl_mm, _np);
  
  //3. self setup
  iC( srcData() );  // see below
  iC( trgData() );  // see below
  
  //-----------------------------------------------------
  return (0);
}
// ---------------------------------------------------------------------- 
int FMM2d::srcData()
{
  //1. create vecs
  int srcNodeCnt = _let->srcNodeCnt();
  int srcExaCnt = _let->srcExaCnt();
  _srcExaPos.resize(dim(), srcExaCnt);
  _srcExaNor.resize(dim(), srcExaCnt);
  _srcExaDen.resize(srcExaCnt * srcDOF());
  _srcUpwEquDen.resize(srcNodeCnt * datSze(UE));  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  _srcUpwChkVal.resize(srcNodeCnt * datSze(UC));  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  //2. gather the Position using the Pos scatter
  vector<int> ordVec;  iC( _let->upwOrderCollect(ordVec) );
  for(int i=0; i<ordVec.size(); i++) {
	 int gNodeIdx = ordVec[i];
	 if(_let->tag(gNodeIdx) & LET_SRCNODE) { //if the node/box with index gNodeIdx is a source contributor/has source(s). Use "&" for testing
		if(_let->terminal(gNodeIdx)==true) { // if this node/box is leaf. //terminal cbtr
		  DblNumMat srcExaPos(this->srcExaPos(gNodeIdx)); // alias
		  DblNumMat srcExaNor(this->srcExaNor(gNodeIdx)); // alias
		  vector<int>& curVecIdxs = _let->node(gNodeIdx).srcOwnVecIdxs(); // alias
		  for(int k=0; k < curVecIdxs.size(); k++) { // select the source position and source normal from the original source and source normal vectors _srcPos and _srcNor. And then put them into the new matrices _srcExaPos and _srcExaNor. 
			 int poff = curVecIdxs[k];
			 for(int d=0; d < dim(); d++) {
				srcExaPos(d,k) = (*_srcPos)(d,poff);// By this, _srcExaPos stores the source positions which has been reordered according to the order the nodes. 
				srcExaNor(d,k) = (*_srcNor)(d,poff);// Nor
			 }
		  }
		}
	 }
  }
  return (0);
}
// ---------------------------------------------------------------------- 
int FMM2d::trgData()
{
  //1. create vecs
  int trgNodeCnt = _let->trgNodeCnt();
  int trgExaCnt = _let->trgExaCnt();
  _trgExaPos.resize(dim(), trgExaCnt);
  _trgExaVal.resize(trgExaCnt * trgDOF());
  _trgDwnEquDen.resize(trgNodeCnt * datSze(DE));
  _trgDwnChkVal.resize(trgNodeCnt * datSze(DC));
  
  //2. gather data from _trgPos
  vector<int> ordVec; iC( _let->upwOrderCollect(ordVec) );
  for(int i=0; i<ordVec.size(); i++) {
	 int gNodeIdx = ordVec[i];
	 if(_let->tag(gNodeIdx) & LET_TRGNODE) {
		if(_let->terminal(gNodeIdx)==true) {
		  DblNumMat trgExaPos(this->trgExaPos(gNodeIdx));
		  vector<int>& curVecIdxs = _let->node(gNodeIdx).trgOwnVecIdxs();
		  for(int k=0; k<curVecIdxs.size(); k++) {
			 int poff = curVecIdxs[k];
			 for(int d=0; d<dim(); d++)
				trgExaPos(d,k) = (*_trgPos)(d, poff);
		  }
		}
	 }
  }
  
  //3. allocate ENExt
  _nodeVec.resize( _let->nodeVec().size() );
  for(int i=0; i<ordVec.size(); i++) {
	 int gNodeIdx = ordVec[i];
	 if(_let->tag(gNodeIdx) & LET_TRGNODE) {
		//V
		Let2d::Node& gg = _let->node(gNodeIdx);
		_nodeVec[gNodeIdx].VinNum() = gg.Vnodes().size();
		for(vector<int>::iterator vi=gg.Vnodes().begin(); vi!=gg.Vnodes().end(); vi++) {
		  _nodeVec[*vi].VotNum() ++;
		}
	 }
  }
  
  return (0);
}
