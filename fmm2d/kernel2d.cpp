/* Kernel Independent Fast Multipole Method
   Copyright (C) 2004 Lexing Ying, New York University

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING.  If not, write to the Free
Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.  */
#include "common/vecmatop.hpp"
#include "kernel2d.hpp"

// ---------------------------------------------------------------------- 
int Kernel2d::srcDOF() const
{
  int dof = 0;
  switch(_kernelType) {
	 //laplace kernels
  case KNL_LAP_S_U: dof = 1; break;
  case KNL_LAP_D_U: dof = 1; break;
  case KNL_LAP_I  : dof = 1; break;
	 //stokes kernels
  case KNL_STK_F_U: dof = 3; break;
  case KNL_STK_S_U: dof = 2; break;
  // case KNL_STK_S_P: dof = 2; break;
  case KNL_STK_D_U: dof = 2; break;
  // case KNL_STK_D_P: dof = 2; break;
  // case KNL_STK_R_U: dof = 2; break;
  // case KNL_STK_R_P: dof = 2; break;
  case KNL_STK_I  : dof = 2; break;
  // case KNL_STK_E  : dof = 2; break;
	 //navier kernels:	 //case KNL_NAV_F_U: dof = 2; break; //used for fmm
  //case KNL_NAV_S_U: dof = 2; break;
  //case KNL_NAV_D_U: dof = 2; break;
  //case KNL_NAV_R_U: dof = 2; break;
  //case KNL_NAV_I  : dof = 2; break;
  //case KNL_NAV_E  : dof = 2; break;
	 //others
  //case KNL_SQRTLAP: dof = 1; break;
  //case KNL_EXP:     dof = 1; break;
	 //error
  case KNL_ERR:     dof = 0; break;
  }
  return dof;
}

// ---------------------------------------------------------------------- 
int Kernel2d::trgDOF() const
{
  int dof = 0;
  switch(_kernelType) {
	 //laplace kernels
  case KNL_LAP_S_U: dof = 1; break;
  case KNL_LAP_D_U: dof = 1; break;
  case KNL_LAP_I  : dof = 1; break;
	 //stokes kernels
  case KNL_STK_F_U: dof = 2; break;
  case KNL_STK_S_U: dof = 2; break;
  //case KNL_STK_S_P: dof = 1; break;
  case KNL_STK_D_U: dof = 2; break;
  //case KNL_STK_D_P: dof = 1; break;
  //case KNL_STK_R_U: dof = 2; break;
  //case KNL_STK_R_P: dof = 1; break;
  case KNL_STK_I  : dof = 2; break;
  //case KNL_STK_E  : dof = 2; break;
	 //navier kernels:	 //  case KNL_NAV_F_U: dof = 2; break; //used for fmm
  //case KNL_NAV_S_U: dof = 2; break;
  //case KNL_NAV_D_U: dof = 2; break;
  //case KNL_NAV_R_U: dof = 2; break;
  //case KNL_NAV_I  : dof = 2; break;
  //case KNL_NAV_E  : dof = 2; break;
	 //others
  //case KNL_SQRTLAP: dof = 1; break;
  //case KNL_EXP    : dof = 1; break;
	 //error
  case KNL_ERR:     dof = 0; break;
  }
  return dof;
}

// ---------------------------------------------------------------------- 
bool Kernel2d::homogeneous() const
{
  bool ret = false;
  switch(_kernelType) {
	 //2D Laplace kernels double layer
  case KNL_LAP_D_U: ret = true; break;
	 //2D Stokes kernels double layer
  //case KNL_STK_D_U: ret = true; break;
	 //2D Navier kernels double layer
  //others：
  //case KNL_SQRTLAP: ret = true; break;
  //case KNL_EXP    : ret = false; break;
  //default: assert(0);
  }
  return ret;
}

// ---------------------------------------------------------------------- 
void Kernel2d::homogeneousDeg(vector<double>& degreeVec) const
{
  switch(_kernelType) {
  case KNL_LAP_D_U: degreeVec.resize(1); degreeVec[0]=1; break;
  // case KNL_STK_D_U: degreeVec.resize(2); degreeVec[0]=1; degreeVec[1]=1; break;
  // case KNL_NAV_D_U: degreeVec.resize(2); degreeVec[0]=1; degreeVec[1]=1; break;
  //others：
  // case KNL_SQRTLAP: degreeVec.resize(1); degreeVec[0]=0.5; break;
  // case KNL_EXP    : degreeVec.resize(0); break;
  default: assert(0);
  }
  return;
}

// kernel function should be split up
// ---------------------------------------------------------------------- 
int Kernel2d::kernel(const DblNumMat& srcPos, const DblNumMat& srcNor, const DblNumMat& trgPos, DblNumMat& inter)
{
  int srcDOF = this->srcDOF();
  int trgDOF = this->trgDOF();
  iA(srcPos.m()==dim() && srcNor.m()==dim() && trgPos.m()==dim());
  iA(srcPos.n()==srcNor.n());
  iA(srcPos.n()*srcDOF == inter.n());
  iA(trgPos.n()*trgDOF == inter.m());
  if(       _kernelType==KNL_LAP_S_U) {
	 //----------------------------------------------------------------------------------
	 double OOFP = 1.0/(2.0*M_PI);
	 for(int i=0; i<trgPos.n(); i++) {
		for(int j=0; j<srcPos.n(); j++) {
		  double x = trgPos(0,i) - srcPos(0,j);
		  double y = trgPos(1,i) - srcPos(1,j);
		  double r2 = x*x + y*y;
		  double r = sqrt(r2);
		  if(r<_mindif) {
			 inter(i,j) = 0;
		  } else {
			 inter(i,j) = -OOFP * log(r);
		  }
		}
	 }
  } else if(_kernelType==KNL_LAP_D_U) {
	 //--------------------------------
	 double OOFP = -1.0/(2.0*M_PI);
	 for(int i=0; i<trgPos.n(); i++)
		for(int j=0; j<srcPos.n(); j++) {
		  double x = trgPos(0,i) - srcPos(0,j);
		  double y = trgPos(1,i) - srcPos(1,j);
                  double r2 = x*x + y*y;
         	  double r = sqrt(r2);
    	  if(r<_mindif) {
                  inter(i, j) = 0;
		  } else {
		 double nx = srcNor(0,j);
		 double ny = srcNor(1,j);
		 double rn = x*nx + y*ny;
		 inter(i,j) = OOFP * rn / r2;
	  }
	}
  } else if(_kernelType==KNL_LAP_I) {
 //---------------------------------
	 for(int i=0; i<trgPos.n(); i++)
        	for(int j=0; j<srcPos.n(); j++)
	              inter(i,j) = 1;
	              
  } else if(_kernelType==KNL_STK_F_U) {
	 //---------------------------------
	 iA(_coefs.size()>=1);
	 double mu = _coefs[0];
	 double OOEP = 1.0/(4.0*M_PI);
	 double oomu = 1.0/mu;
	 for(int i=0; i<trgPos.n(); i++)
		for(int j=0; j<srcPos.n(); j++) {
		  double x = trgPos(0,i) - srcPos(0,j);
		  double y = trgPos(1,i) - srcPos(1,j);
		  double r2 = x*x + y*y;
		  double r = sqrt(r2);
		  if(r<_mindif) {
			 for(int t=0;t<trgDOF;t++)
				for(int s=0;s<srcDOF;s++)
				  { inter(i*trgDOF+t, j*srcDOF+s) = 0.0; }
		  } else {
			 double G = -OOEP * log(r);
			 double H = OOEP / r2;
			 double A = OOEP / r2;
			 int is = i*2;			 int js = j*3;
			 inter(is,   js) = oomu*(G + H*x*x); inter(is,   js+1) = oomu*(    H*x*y); inter(is,   js+2) = 2*A*x;
			 inter(is+1, js) = oomu*(    H*y*x); inter(is+1, js+1) = oomu*(G + H*y*y); inter(is+1, js+2) = 2*A*y;

		  }
		}        
	              
} else if(_kernelType==KNL_STK_S_U) {
	 //---------------------------------
	 iA(_coefs.size()>=1);
	 double mu = _coefs[0];
         double OOEP = 1.0/(4.0*M_PI);
         double oomu = 1.0/mu;
	 for(int i=0; i<trgPos.n(); i++)
		for(int j=0; j<srcPos.n(); j++) {
		  double x = trgPos(0,i) - srcPos(0,j);
		  double y = trgPos(1,i) - srcPos(1,j);
		  double r2 = x*x + y*y;
		  double r = sqrt(r2);
		  if(r<_mindif) {
			 for(int t=0;t<trgDOF;t++)
				for(int s=0;s<srcDOF;s++)
				  { inter(i*trgDOF+t, j*srcDOF+s) = 0.0; }
		  } else {
			 double G = -OOEP * log(r);
			 double H = OOEP / r2;

			 inter(i*2,   j*2)   = oomu*(G + H*x*x); inter(i*2,   j*2+1) = oomu*(    H*x*y);
			 inter(i*2+1, j*2)   = oomu*(    H*y*x); inter(i*2+1, j*2+1) = oomu*(G + H*y*y);
		  }
		}
 } else if(_kernelType==KNL_STK_D_U) {
	 //---------------------------------
	 iA(_coefs.size()>=1);
	 double mu = _coefs[0];
	 double SOEP = 1.0/M_PI;
	 for(int i=0; i<trgPos.n(); i++)
		for(int j=0; j<srcPos.n(); j++) {
		  double x = trgPos(0,i) - srcPos(0,j);
		  double y = trgPos(1,i) - srcPos(1,j);
          double r2 = x*x + y*y;
		  double r = sqrt(r2);
		  if(r<_mindif) {
			 for(int t=0;t<trgDOF;t++)
				for(int s=0;s<srcDOF;s++)
				  { inter(i*trgDOF+t, j*srcDOF+s) = 0.0; }
		  } else {
			 double nx = srcNor(0,j);
			 double ny = srcNor(1,j);
			 double rn = x*nx + y*ny;
			 double r4 = r2*r2;
			 double C = - SOEP / r4;			 
			 		 
			 inter(i*2,   j*2)   = C*rn*x*x;	inter(i*2,   j*2+1) = C*rn*x*y;
			 inter(i*2+1, j*2)   = C*rn*y*x;    inter(i*2+1, j*2+1) = C*rn*y*y;
		  }
		}
  } else if(_kernelType==KNL_STK_I) {
	 //---------------------------------
	 for(int i=0; i<trgPos.n(); i++)
		for(int j=0; j<srcPos.n(); j++) {
		  inter(i*2,   j*2  ) = 1;		 inter(i*2,   j*2+1) = 0;
		  inter(i*2+1, j*2  ) = 0;		 inter(i*2+1, j*2+1) = 1;
	        } 
} else if(_kernelType==KNL_ERR) {
	 //----------------------------------------------------------------------------------
	 iA(0);
  }
  return 0;
}
