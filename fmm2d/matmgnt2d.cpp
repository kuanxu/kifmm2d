/* Kernel Independent Fast Multipole Method
   Copyright (C) 2004 Lexing Ying, New York University

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING.  If not, write to the Free
Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.  */
#include "matmgnt2d.hpp"
#include "common/vecmatop.hpp"

using std::cerr;
using std::endl;

// ---------------------------------------------------------------------- 
double MatMgnt2d::_wsbuf[16384];

// ---------------------------------------------------------------------- 
MatMgnt2d::MatMgnt2d(): _np(16)
{
}
// ---------------------------------------------------------------------- 
MatMgnt2d::~MatMgnt2d()
{
}

double MatMgnt2d::alt()
{
  return pow(0.1, floor(double(_np)/3.0));
}

// ---------------------------------------------------------------------- 
int MatMgnt2d::setup()
{
  _hom = _knl.homogeneous();
  if(_hom==true) {
	 _knl.homogeneousDeg(_degVec); iA(_degVec.size()==srcDOF());
  }
  
  double d = 0.01;
  iC( samPosCal(_np,   sqrt(2.0)+d      , _samPos[UE]) );
  iC( samPosCal(_np,   4.0-sqrt(2.0)-2*d, _samPos[UC]) );
  iC( samPosCal(_np,   4.0-sqrt(2.0)-2*d, _samPos[DE]) );
  iC( samPosCal(_np,   sqrt(2.0)+d      , _samPos[DC]) );
  return (0);
}
// ---------------------------------------------------------------------- 
int MatMgnt2d::report()
{
  cerr << "matrix map size"<<endl;
  cerr << _upwEqu2UpwChk.size() << " ";
  cerr << _upwChk2UpwEqu.size() << " ";
  cerr << _dwnChk2DwnEqu.size() << " ";
  cerr << _dwnEqu2DwnChk.size() << " ";
  cerr << _upwEqu2DwnChk.size() << endl;
  return (0);
}

// ---------------------------------------------------------------------- 
int MatMgnt2d::plnDatSze(int tp)
{
  if(tp==UE || tp==DE)
	 return _samPos[tp].n()*srcDOF();
  else
	 return _samPos[tp].n()*trgDOF();
}

// ---------------------------------------------------------------------- 
int MatMgnt2d::UpwChk2UpwEqu_dgemv(int l, const DblNumVec& chk, DblNumVec& den)
    //                             input        input             output
{
  DblNumMat& _UC2UE = (_hom==true) ? _upwChk2UpwEqu[0] : _upwChk2UpwEqu[l];
  double R = (_hom==true) ? 1 : 1.0/pow(2.0,l);
  //---------compute matrix
  if(_UC2UE.m()==0) {	 //cerr<<"UpwChk2UpwEqu compute"<<endl;
	 //set matrix
	 DblNumMat ud2c(plnDatSze(UC), plnDatSze(UE));
	 DblNumMat chkPos(dim(),samPos(UC).n());	 clear(chkPos);	 iC( daxpy(R, samPos(UC), chkPos) ); //scale
	 DblNumMat denPos(dim(),samPos(UE).n());	 clear(denPos);	 iC( daxpy(R, samPos(UE), denPos) ); //scale
	 
	 iC( _knl.kernel(denPos, denPos, chkPos, ud2c) );
	 _UC2UE.resize(plnDatSze(UE), plnDatSze(UC));
	 iC( pinv(ud2c, alt(), _UC2UE) );
  }
  //---------matvec
  if(_hom==true) {
	 //matvec
	 int srcDOF = this->srcDOF();
	 DblNumVec tmpDen(srcDOF*samPos(UE).n(), false, _wsbuf);	 clear(tmpDen);
	 iC( dgemv(1.0, _UC2UE, chk, 1.0, tmpDen) );
	 //scale
	 vector<double> sclvec(srcDOF);	 for(int s=0; s<srcDOF; s++)		sclvec[s] = pow(2.0, - l*_degVec[s]);
	 int cnt = 0;
	 for(int i=0; i < samPos(UE).n(); i++)
		for(int s=0; s < srcDOF; s++) {
		  den(cnt) = den(cnt) + tmpDen(cnt) * sclvec[s];
		  cnt++;
		}
  } else {
	 iC( dgemv(1.0, _UC2UE, chk, 1.0, den) );
  }
  return (0);
}
// ---------------------------------------------------------------------- 
int MatMgnt2d::UpwEqu2UpwChk_dgemv(int l, Index2 idx, const DblNumVec& den, DblNumVec& chk)
{
  NumMat<DblNumMat>& _UE2UC = (_hom==true) ? _upwEqu2UpwChk[0] : _upwEqu2UpwChk[l];  // alias
  double R = (_hom==true) ? 1 : 1.0/pow(2.0, l);  
  if(_UE2UC.m()==0)	 _UE2UC.resize(2,2);
  DblNumMat& _UE2UCii = _UE2UC(idx(0), idx(1));   // alias
  //---------compute matrix
  if(_UE2UCii.m()==0) {	 //cerr<<"UpwEqu2UpwChk compute"<<endl;
	 _UE2UCii.resize(plnDatSze(UC), plnDatSze(UE)); //_memused[1] += plnnum(UC)*dof()*plnnum(UE)*dof()*sizeof(double);
	 DblNumMat chkPos(dim(),samPos(UC).n());	 clear(chkPos);	 iC( daxpy(2.0*R, samPos(UC), chkPos) ); //scale
	 DblNumMat denPos(dim(),samPos(UE).n());	 clear(denPos);	 iC( daxpy(R, samPos(UE), denPos) ); //scale
	 for(int i=0; i<dim(); i++) for(int j=0; j<samPos(UE).n(); j++)	denPos(i,j) = denPos(i,j) + (2*idx(i)-1)*R;//shift
	 
	 iC( _knl.kernel(denPos, denPos, chkPos, _UE2UCii) );
  }
  //---------matvec
  if(_hom==true) {
	 int srcDOF = this->srcDOF();
	 DblNumVec tmpDen(srcDOF*samPos(UE).n(), false, _wsbuf);	 clear(tmpDen);
	 vector<double> sclvec(srcDOF);	 for(int s=0; s<srcDOF; s++)		sclvec[s] = pow(2.0, l*_degVec[s]);
	 int cnt = 0;
	 for(int i=0; i<samPos(UE).n(); i++)
		for(int s=0; s<srcDOF; s++) {
		  tmpDen(cnt) = den(cnt) * sclvec[s];
		  cnt++;
		}
	 iC( dgemv(1.0, _UE2UCii, tmpDen, 1.0, chk) );
  } else {
	 iC( dgemv(1.0, _UE2UCii, den, 1.0, chk) );
  }
  return (0);
}

// ---------------------------------------------------------------------- 
int MatMgnt2d::DwnChk2DwnEqu_dgemv(int l, const DblNumVec& chk, DblNumVec& den)
{
  DblNumMat& _DC2DE = (_hom==true) ? _dwnChk2DwnEqu[0]: _dwnChk2DwnEqu[l];
  double R = (_hom==true) ? 1 : 1.0/pow(2.0,l);
  //---------compute matrix
  if(_DC2DE.m()==0) {	 //cerr<<"DwnChk2DwnEqu compute"<<endl;
	 DblNumMat dd2c(plnDatSze(DC), plnDatSze(DE));
	 DblNumMat chkPos(dim(),samPos(DC).n());		clear(chkPos);	 iC( daxpy(R, samPos(DC), chkPos) ); //scale
	 DblNumMat denPos(dim(),samPos(DE).n());		clear(denPos);	 iC( daxpy(R, samPos(DE), denPos) ); //scale
	 
	 iC( _knl.kernel(denPos, denPos, chkPos, dd2c) );//matrix
	 _DC2DE.resize(plnDatSze(DE), plnDatSze(DC)); //_memused[2] += plndnenum()*dof()*plndncnum()*dof()*sizeof(double);
	 iC( pinv(dd2c, alt(), _DC2DE) );
  }
  //---------matvec
  if(_hom==true) {
	 int srcDOF = this->srcDOF();
	 DblNumVec tmpDen(srcDOF*samPos(DE).n(), false, _wsbuf);	 clear(tmpDen);
	 iC( dgemv(1.0, _DC2DE, chk, 1.0, tmpDen) );
	 //scale
	 vector<double> sclvec(srcDOF);	 for(int s=0; s<srcDOF; s++)		sclvec[s] = pow(2.0, - l*_degVec[s]);
	 int cnt = 0;
	 for(int i=0; i<samPos(DE).n(); i++)
		for(int s=0; s<srcDOF; s++) {
		  den(cnt) = den(cnt) + tmpDen(cnt) * sclvec[s];
		  cnt++;
		}
  } else {
	 iC( dgemv(1.0, _DC2DE, chk, 1.0, den) );
  }
  return 0;
}
// ---------------------------------------------------------------------- 
int MatMgnt2d::DwnEqu2DwnChk_dgemv(int l, Index2 idx, const DblNumVec& den, DblNumVec& chk)
{
  NumMat<DblNumMat>& _DE2DC = (_hom==true) ? _dwnEqu2DwnChk[0] : _dwnEqu2DwnChk[l];
  double R = (_hom==true) ? 1 : 1.0/pow(2.0, l);  //OffTns<DblNumMat>& _DwnEqu2DwnChk = _de2DwnChk[l];  double R       = 1.0/pow(2.0, l);
  if(_DE2DC.m()==0)	 _DE2DC.resize(2,2);
  DblNumMat& _DE2DCii = _DE2DC(idx[0], idx[1]);
  
  //---------compute matrix
  if(_DE2DCii.m()==0) {	 //cerr<<"DwnEqu2DwnChk compute"<<endl;
	 _DE2DCii.resize(plnDatSze(DC), plnDatSze(DE)); //_memused[3] += plndncnum()*dof()*plndnenum()*dof()*sizeof(double);
	 DblNumMat denPos(dim(),samPos(DE).n());		  clear(denPos);	 iC( daxpy(R, samPos(DE), denPos) ); //scale
	 DblNumMat chkPos(dim(),samPos(DC).n());		  clear(chkPos);	 iC( daxpy(0.5*R, samPos(DC), chkPos) ); //scale
	 for(int i=0; i<dim(); i++) for(int j=0; j<samPos(DC).n(); j++) chkPos(i,j) = chkPos(i,j) + (double(idx(i))-0.5)*R; //shift
	 
	 iC( _knl.kernel(denPos, denPos, chkPos, _DE2DCii) );
  }
  //---------matvec
  if(_hom==true) {
	 int srcDOF = this->srcDOF();
	 DblNumVec tmpDen(srcDOF*samPos(DE).n(), false, _wsbuf);	 clear(tmpDen);
	 vector<double> sclvec(srcDOF);	 for(int s=0; s<srcDOF; s++)		sclvec[s] = pow(2.0, l*_degVec[s]);
	 int cnt = 0;
	 for(int i=0; i<samPos(DE).n(); i++)
		for(int s=0; s<srcDOF; s++) {
		  tmpDen(cnt) = den(cnt) * sclvec[s];
		  cnt++;
		}
	 iC( dgemv(1.0, _DE2DCii, tmpDen, 1.0, chk) );
  } else {
	 iC( dgemv(1.0, _DE2DCii, den, 1.0, chk) );
  }
  return (0);
}


// -----------------------UE2DC-------------------------------- 
int MatMgnt2d::UpwEqu2DwnChk_dgemv(int l, Index2 idx, const DblNumVec& Den, DblNumVec& chk)
{
  OffMat<SVDRep>& _UpwEqu2DwnChk = (_hom==true) ? _upwEqu2DwnChk[0] : _upwEqu2DwnChk[l];  //alias
  double R = (_hom==true) ? 1.0 : 1.0/pow(2.0, l);
  if(_UpwEqu2DwnChk.m()==0)	 _UpwEqu2DwnChk.resize(7,7,-3,-3);
  SVDRep& _UpwEqu2DwnChkii = _UpwEqu2DwnChk(idx[0], idx[1]);  // alias
  
  int srcDOF = this->srcDOF();
  int trgDOF = this->trgDOF();
  
  //---------compute matrix
  if(_UpwEqu2DwnChkii.U().m()==0) { //compute it if necessary
 
	 iA( idx.linfty()>1 );
	 DblNumMat denPos(dim(),samPos(UE).n());	clear(denPos); iC( daxpy(R, samPos(UE), denPos) );
	 DblNumMat chkPos(dim(),samPos(DC).n());	clear(chkPos); iC( daxpy(R, samPos(DC), chkPos) );
	 for(int i=0; i<dim(); i++) for(int j=0; j<samPos(UE).n(); j++) denPos(i,j) = denPos(i,j) + double(idx(i))*2.0*R; //shift
	 	 
	 DblNumMat _UE2DCii(plnDatSze(DC),plnDatSze(UE));    clear(_UE2DCii);
	 iC( _knl.kernel(denPos, denPos, chkPos, _UE2DCii) );  //form the M2L matrix
	 
	 double epsilon = pow(0.1,7);
	 iC( _UpwEqu2DwnChkii.construct(epsilon, _UE2DCii) );
	 cerr << "number of singular value r is " << _UpwEqu2DwnChkii.S().m() << " compared to the maximum of m and n "<< max(_UpwEqu2DwnChkii.U().m(), _UpwEqu2DwnChkii.U().n()) << endl;
	 }
	 
	 // ------ matvec
	 if(_hom==true) {
	 DblNumVec tmpDen(srcDOF*samPos(UE).n(), false, _wsbuf);	 clear(tmpDen);
	 vector<double> sclvec(srcDOF);	 for(int s=0; s<srcDOF; s++)		sclvec[s] = pow(2.0, l*_degVec[s]);
	 int cnt = 0;
	 for(int i=0; i<samPos(DE).n(); i++)
		for(int s=0; s<srcDOF; s++) {
		  tmpDen(cnt) = Den(cnt) * sclvec[s];
		  cnt++;
		}
	 iC( _UpwEqu2DwnChkii.dgemv(1.0, tmpDen, 1.0, chk, 0.0) );
     } else {
     iC( _UpwEqu2DwnChkii.dgemv(1.0, Den, 1.0, chk, 0.0) );
     }
return (0);
}

// ---------------------------------------------------------------------- 
int MatMgnt2d::localPos(int tp, Point2 center, double radius, DblNumMat& positions)    // tp is type
{
  const DblNumMat& bas = samPos(tp);
  positions.resize(dim(), bas.n());
  for(int i=0; i<dim(); i++)
	 for(int j=0; j<positions.n(); j++)
		positions(i,j) = center(i) + radius * bas(i,j);
  return (0);
}

// ---------------------------------------------------------------------- 
// position on the circle with radius R starting from (1,0)
int MatMgnt2d::samPosCal(int np, double R, DblNumMat& positions)
{
  positions.resize(dim(),np);
  double step = 2.0*M_PI/np;

  int cnt = 0;
  for(int i=0; i<np; i++){
	positions(0,cnt) = R*cos(i*step);
	positions(1,cnt) = R*sin(i*step);
	cnt++;
	}
  iA(cnt==np);
  return 0;
}
//-----------------------------------------------------------------------------
vector<MatMgnt2d> MatMgnt2d::_mmvec;

MatMgnt2d* MatMgnt2d::getmmptr(Kernel2d knl, int np)
{
  for(int i=0; i<_mmvec.size(); i++)
	 if(_mmvec[i].knl()==knl && _mmvec[i].np()==np)
		return &(_mmvec[i]);
  
  _mmvec.push_back( MatMgnt2d() );
  int last = _mmvec.size()-1;
  MatMgnt2d* tmp = &(_mmvec[last]); //get the last one
  tmp->knl() = knl;  tmp->np() = np;
  tmp->setup();
  return tmp;
}
